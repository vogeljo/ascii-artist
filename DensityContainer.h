#pragma once
#include <algorithm>
#include <vector>

struct DensityCharToken {
  float density;
  char c;
};

class DensityContainer {
private:
  float max_density;
  std::vector<DensityCharToken> tokens;
public:
  DensityContainer() {

  }

  ~DensityContainer() {

  }

  void add(char c, float density) {
    DensityCharToken t = {density, '\0'};

    auto closest = std::lower_bound(tokens.begin(), tokens.end(), t, [](DensityCharToken c1, DensityCharToken c2) {
      return c1.density < c2.density;
    });

    DensityCharToken nToken = { density, c };

    tokens.insert(closest, nToken);

    this->max_density = std::max(density, this->max_density);
  }

  char getCharForBrightness(float b) {
    b *= max_density;

    DensityCharToken t = {b, '\0'};

    auto closest = std::lower_bound(tokens.begin(), tokens.end(), t, [](DensityCharToken c1, DensityCharToken c2) {
      return c1.density < c2.density;
    });

    if (closest == tokens.end())
      return '\0';
    else
      return closest->c;
  }
};
