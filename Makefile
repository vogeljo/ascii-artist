BINARY_NAME=./ascii-artist

all: *.cpp
	g++ -std=c++11 *.cpp -o $(BINARY_NAME) -lpng -lfreetype -I/usr/include/freetype2 -I/usr/include/libpng12

run: all
	$(BINARY_NAME)

clean:
	rm -rf $(BINARY_NAME)
