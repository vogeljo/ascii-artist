#pragma once

template<typename T>
class averager {
private:
  T accu;
  int len;
public:
  averager() : len(0), accu(T()) {

  }

  ~averager() {

  }

  void push(T value) {
    T old_accu = accu;
    accu = (len * old_accu + value) / (++len);
  }

  T get() {
    return accu;
  }
};

typedef averager<float> averager_f;
