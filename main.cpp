#include <png.h>

#include <algorithm>
#include <iostream>
#include <fstream>

#include <math.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>

#include "avg.h"
#include "DensityContainer.h"

#include <ft2build.h>
#include FT_FREETYPE_H

#define VERSION_STRING "1.0"

void myabort(const char *message, ...) {
	printf("Error: ");
	va_list args;
	va_start(args, message);
	vfprintf(stdout, message, args);
	va_end(args);
	printf("\n");
	exit(1);
}

DensityContainer densities;

void init_font_densities(char *font_file) {
	printf("Analyzing ASCII glyphs... ");

	FT_Library ftlib;
	auto error = FT_Init_FreeType(&ftlib);
	if (error) {
		printf("Error initializing freetype.");
	}

	FT_Face face;

	error = FT_New_Face(ftlib, font_file, 0, &face);
	if (error)
		myabort("Error loading font file.\n");

	error = FT_Set_Char_Size(face, 0, 16*64, 300, 300);


	for (int glyph_n = 0; glyph_n < 256; ++glyph_n) {
		char glyph_char = (char)glyph_n;

		if (iscntrl(glyph_char))
		continue;

		auto glyph_index = FT_Get_Char_Index(face, glyph_char);

		if (glyph_index == 0)
		continue;

		error = FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT);

		error = FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);

		auto slot = face->glyph;
		auto bitmap = slot->bitmap;

		if (bitmap.pixel_mode != FT_PIXEL_MODE_GRAY)
		myabort("Wrong pixel format in glyph bitmap (%d) [%d].", bitmap.pixel_mode, error);

		averager_f avg_glyph;

		for (int gy = 0; gy < bitmap.rows; ++gy) {
			for (int gx = 0; gx < bitmap.width; ++gx) {
				auto value = float(bitmap.buffer[gy * bitmap.width + gx]) / 256.0f;
				avg_glyph.push(value);
			}
		}

		auto glyph_density = bitmap.width * bitmap.rows * avg_glyph.get();

		densities.add(glyph_char, glyph_density);
	}

	printf("done.\n");
}

void print_help_line(char option, const char *description, const char *default_value = NULL) {
	printf("  -%c\t\t%s\n", option, description);
	if (default_value)
		printf("    \t\tDefaults to %s\n", default_value);
}

int main(int argc, char **argv) {
	const char *default_image_file = "image.png";
	const char *default_output_file = "ascii_art.txt";
	const char *default_font_file = "/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf";

	char *font_file = (char*)default_font_file;
	char *image_file = (char*)default_image_file;
	char *output_file = (char*)default_output_file;
	int block_width = 2;
	bool verbose = false;
	int c = 0;

	opterr = 0;

	while ((c = getopt(argc, argv, "w:f:i:o:hv")) != -1) {
		switch(c) {
			case 'f':
				font_file = optarg;
				break;
			case 'w':
				block_width = atoi(optarg);
				break;
			case 'i':
				image_file = optarg;
				break;
			case 'o':
				output_file = optarg;
				break;
			case 'v':
				verbose = true;
				break;
			case 'h':
				printf("ascii-artist v%s\nA simple PNG to ascii art 'converter'.\n\n", VERSION_STRING);
				printf("Options:\n");
				print_help_line('f', "Specifies the font file to use for glyph analysis.", default_font_file);
				print_help_line('w', "Block width to use (default is 2). Block height will be 3/2 of block width.");
				print_help_line('i', "Image file to use. Currently, ascii-artist only supports PNG images.", default_image_file);
				print_help_line('o', "Output text file to use.", default_output_file);
				print_help_line('v', "Verbose output.");
				print_help_line('h', "Print this help message.");
				return 0;
			case '?':
			case ':':
			default:
				myabort("Missing argument for or unknown option -%c.\nTry ascii-artist -h for help.\n", optopt);
				break;
		}
	}

	init_font_densities(font_file);

	int block_height = block_width * 2 + (block_width / 2);

	printf("Block size will be %d x %d pixels.\n", block_width, block_height);

	FILE *fp = fopen(image_file, "rb");
	if (!fp)
	myabort("Could not open image file '%s'.", image_file);

	png_struct *png_ptr = nullptr;
	png_info *png_info = nullptr;

	png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

	png_info = png_create_info_struct(png_ptr);

	png_init_io(png_ptr, fp);

	png_read_info(png_ptr, png_info);

	int width = png_get_image_width(png_ptr, png_info);
	int height = png_get_image_height(png_ptr, png_info);

	printf("Image dimensions: %d x %d\n", width, height);

	auto n_alloc = sizeof(png_bytep) * height;

	auto row_pointers = (png_bytep*)malloc(n_alloc);

	for (int y = 0; y <  height; ++y) {
		row_pointers[y] = (png_byte*)malloc(png_get_rowbytes(png_ptr, png_info));
	}

	png_read_image(png_ptr, row_pointers);

	bool has_alpha_channel = false;

	switch(png_get_color_type(png_ptr, png_info)) {
		case PNG_COLOR_TYPE_RGB:
		break;
		case PNG_COLOR_TYPE_RGBA:
		has_alpha_channel = true;
		break;
		default:
		myabort("PNG has wrong color type.\n");
		break;
	}

	int n_bx = width / block_width;
	int n_by = height / block_height;

	std::ofstream ofs(output_file);

	printf("Writing to file '%s', %d columns and %d rows.\n", output_file, n_bx, n_by);

	int bytes_per_pixel = has_alpha_channel ? 4 : 3;

	for (int by = 0; by < n_by; ++by) {
		for (int bx = 0; bx < n_bx; ++bx) {
			averager_f avg_r, avg_b, avg_g, avg_a;
			for (int y = 0; y < block_height; ++y) {
				for (int x = 0; x < block_width; ++x) {
					int
					img_x = bx * block_width + x,
					img_y = by * block_height + y;

					auto rgba = &row_pointers[img_y][img_x * bytes_per_pixel];
					float
					r_f = float(rgba[0]) / 255.0f,
					g_f = float(rgba[1]) / 255.0f,
					b_f = float(rgba[2]) / 255.0f,
					a_f = 1.0f;

					if (has_alpha_channel)
					a_f = float(rgba[3]) / 255.0f;

					avg_r.push(r_f);
					avg_g.push(g_f);
					avg_b.push(b_f);
					avg_a.push(a_f);
				}
			}
			//printf("Average values [R,G,B]: %f, %f, %f\n", avg_r.get(), avg_g.get(), avg_b.get());

			float r = avg_r.get(), g = avg_g.get(), b = avg_b.get(), a = avg_a.get();

			//float brightness = a * (r + g + b) / 3;
			float brightness = 1.0f - a * sqrt(0.299f * r*r + 0.587*g*g + 0.114*b*b);

			if (verbose)
				printf("Brightness for block [%d,%d] starting at (%d, %d): %f\n", bx, by, bx * block_width, by * block_height, brightness);

			ofs << densities.getCharForBrightness(brightness);
		}
		ofs << '\n';
	}

	printf("Done.\n");

	fclose(fp);

	return 0;
}
